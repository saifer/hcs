<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%customer_issue}}`.
 */
class m190928_115419_create_customer_issue_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%customer_issue}}', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull(),
            'status' => $this->string()->notNull(),
            'trouble_id' => $this->integer()->notNull(),
            'comment' => $this->text(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%customer_issue}}');
    }
}
