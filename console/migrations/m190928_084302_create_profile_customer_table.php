<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%profile_customer}}`.
 */
class m190928_084302_create_profile_customer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%profile_customer}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'third_name' => $this->string(),
            'phone' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
        ]);

        $this->createIndex(
            'profile_customer__user_id__phone__index',
            '{{%profile_customer}}',
            [
                'user_id',
                'phone',
            ],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%profile_customer}}');
    }
}
