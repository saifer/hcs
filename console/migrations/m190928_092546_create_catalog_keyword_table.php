<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalog_keyword}}`.
 */
class m190928_092546_create_catalog_keyword_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalog_keyword}}', [
            'catalog_id' => $this->integer()->notNull(),
            'text' => $this->string()->notNull(),
        ]);

        $this->createIndex(
            'catalog_keyword__catalog_id__text__index',
            '{{%catalog_keyword}}',
            [
                'catalog_id',
                'text',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalog_keyword}}');
    }
}
