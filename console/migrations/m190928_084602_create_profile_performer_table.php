<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%profile_performer}}`.
 */
class m190928_084602_create_profile_performer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%profile_performer}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'third_name' => $this->string(),
            'phone' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
        ]);

        $this->createIndex(
            'profile_performer__user_id__phone__index',
            '{{%profile_performer}}',
            [
                'user_id',
                'phone',
            ],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%profile_performer}}');
    }
}
