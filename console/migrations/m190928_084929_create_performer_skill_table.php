<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%performer_skill}}`.
 */
class m190928_084929_create_performer_skill_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%performer_skill}}', [
            'profile_performer_id' => $this->integer()->notNull(),
            'skill_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
        ]);

        $this->createIndex(
            'performer_skill__profile_performer_id__skill_id__index',
            '{{%performer_skill}}',
            [
                'profile_performer_id',
                'skill_id',
            ],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%performer_skill}}');
    }
}
