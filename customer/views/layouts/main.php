<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

$user = Yii::$app->user;
/** @var User $userModel */
$userModel = $user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <title>#просто ЖКХ - <?= Html::encode($this->title) ?></title>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="description" content="#просто ЖКХ">
    <meta name="keywords" content="">
    <meta name="author" content="Команда Сайфер">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#0000DD">
    <?php $this->registerCsrfMetaTags() ?>

    <!--<link rel="shortcut icon" type="images/png" href="images/favicon/logo.png">
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-touch-icon-114x114.png">-->
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap&subset=cyrillic" rel="stylesheet">

    <link rel="stylesheet" href="styles/core.css">
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

<div class="content">
    <header>
        <div class="mainBar">
            <div class="logoBar" id="logoBar"><img src="images/logo.svg"><h5>#просто<br>ЖКХ</h5></div>
            <div class="menuButton" id="menuButton">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>

        <?php if ($user->isGuest) { ?>
            <div class="userBar" id="userBar"><p>Неавторизованный пользователь</p></div>
        <?php } else { ?>
            <div class="userBar" id="userBar"><p><?= $userModel->first_name . ' ' . $userModel->last_name ?></p></div>
        <?php } ?>
        <div class="menuBar" id="menuBar">
            <h5>Навигация</h5>
            <a href="index"><u>01 | Главная</u></a>
            <?php if (Yii::$app->user->isGuest) { ?>
                <a href="<?=Url::to(['login']);?>"><u>02 | Вход</u></a>
                <a href="<?=Url::to(['signup']);?>"><u>03 | Регистрация</u></a>
            <?php } else { ?>
                <a href="<?=Url::to(['private-office']);?>"><u>02 | Личный кабинет</u></a>
                <a href="<?=Url::to(['logout']);?>"><u>03 | Выход</u></a>
            <?php } ?>
            <a href="reg"><u>04 | О сервисе</u></a>
            <a href="reg"><u>05 | Часто задаваемые вопросы</u></a>
        </div>
    </header>

    <?= $content ?>

    <footer>
    </footer>
</div>

<script src="js/jquery.min.js"></script>
<script src='js/core.js'></script>
<script src='js/bot.js'></script>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
