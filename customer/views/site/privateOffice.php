<?php

/* @var $this yii\web\View */
/* @var $issueList CustomerIssue[] */

use common\components\customerIssue\models\CustomerIssue;
use common\helpers\DateHelper;
use yii\helpers\Html;

$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userIssues">
<h3><?= Html::encode($this->title) ?></h3>
    <div class="issuesPanel">
<?php foreach ($issueList as $issue) { ?>
    <div class="issueCard">
        <div>
            <p>#<?=$issue->id;?></p>
            <p><?=DateHelper::format($issue->created_at);?></p>
        </div>
        <div>
            <h5><?=$issue->catalog->parentCategory->title;?></h5>
            <p><?=$issue->catalog->title;?></p>
        </div>
        <div>
            <div class="<?=$issue->getHtmlClass();?>"></div>
            <b><?=$issue->translateStatus();?></b>
        </div>
    </div>
<?php } ?>
</div>
</div>
