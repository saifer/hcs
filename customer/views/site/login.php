<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Вход в аккаунт';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrapper">
    <h2><?= Html::encode($this->title) ?></h2>

           <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'class' => 'inputTxt', 'placeholder' => true])->label(false) ?>

                <?= $form->field($model, 'password')->passwordInput(['placeholder' => true, 'class' => 'inputTxt'])->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton('Войти', ['class' => 'buttonV', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
</div>
