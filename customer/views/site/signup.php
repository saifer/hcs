<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \customer\models\SignupForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrapper">
    <h3><?= Html::encode($this->title) ?></h3>
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'email')->textInput(['placeholder' => true, 'class' => 'inputTxt'])->label(false) ?>

                <?= $form->field($model, 'password')->passwordInput(['placeholder' => true, 'class' => 'inputTxt'])->label(false) ?>

                <?= $form->field($model, 'first_name')->textInput(['placeholder' => true, 'class' => 'inputTxt'])->label(false) ?>

                <?= $form->field($model, 'last_name')->textInput(['placeholder' => true, 'class' => 'inputTxt'])->label(false) ?>

                <?= $form->field($model, 'phone')
                    ->widget(
                        MaskedInput::class, [
                            'mask' => '+7 (999) 999-99-99',
                            'options' => [
                                'class' => 'inputTxt',
                                'id' => 'phone',
                                'placeholder' => true
                            ],
                            'clientOptions' => [
                                'clearIncomplete' => true
                            ]
                        ]
                    )->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'buttonV', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
</div>
