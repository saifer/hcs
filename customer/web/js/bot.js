$(document).ready( getMsgs() );

function getMsgs(){
    $.ajax({
        type: "GET",
        url: "v1/message",
        dataType: 'json',
        success: function(data) {
            var newIncomingMsg;
            $.each(data['response'], function(key, value){
                if(value['is_self']){
                    newIncomingMsg = '<div class="msgOut" id="'+ value['id'] +'"><div><p>'+ value['created_at'] +'</p><p>'+ value['username'] +'</p></div><p>'+ value['text'] +'</p></div>';
                }else{
                    newIncomingMsg = '<div class="msgIn" id="'+ value['id'] +'"><div><p>'+ value['username'] +'</p><p>'+ value['created_at'] +'</p></div><p>'+ value['text'] +'</p></div>';
                }
                $('#msgLog').append(newIncomingMsg);
            })
        }
        })
}

$('#sendMsg').click(function() {
    var message = $('#message').val();
    $.ajax({
        type: "POST",
        url: "v1/message",
        data: {'text':message},
        dataType: 'json',
        success: function(data) {
            var newMsg = '<div class="msgOut" id="'+ data['response']['id'] +'"><div><p>'+ data['response']['created_at'] +'</p><p>'+ data['response']['username'] +'</p></div><p>'+ data['response']['text'] +'</p></div>';
                $('#msgLog').append(newMsg);
        }
    });
});

function getSuggestions(){
    $.ajax({
        type: "GET",
        url: "v1/message",
        dataType: 'json',
        success: function(data) {
            var newIncomingMsg;
            $.each(data['response'], function(key, value){
                if(value['is_self']){
                    newIncomingMsg = '<div class="msgOut" id="'+ value['id'] +'"><div><p>'+ value['created_at'] +'</p><p>'+ value['username'] +'</p></div><p>'+ value['text'] +'</p></div>';
                }else{
                    newIncomingMsg = '<div class="msgIn" id="'+ value['id'] +'"><div><p>'+ value['username'] +'</p><p>'+ value['created_at'] +'</p></div><p>'+ value['text'] +'</p></div>';
                }
                $('#msgLog').append(newIncomingMsg);
            })
        }
    })
}