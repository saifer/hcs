menu = 0;

$('#menuButton').click(function(){
    if (menu == 0){
        $('.menuButton>div:nth-child(1)').css('top', '22px').css('transform', 'rotateZ(45deg)');
        $('.menuButton>div:nth-child(2)').hide(100);
        $('.menuButton>div:nth-child(3)').css('top', '22px').css('transform', 'rotateZ(-45deg)');
        $('html').css('overflow','hidden');
        $('html').css('position','fixed');
        $('html').css('width','100%');
        $('#userBar').css('display', 'flex');
        $('#userBar').show(200);
        $('#menuBar').css('display', 'flex');
        $('#menuBar').show(200);
        menu = 1;
    } else{
        $('.menuButton>div:nth-child(1)').css('top', '8px').css('transform', 'rotateZ(0)');
        $('.menuButton>div:nth-child(2)').show(100);
        $('.menuButton>div:nth-child(3)').css('top', '34px').css('transform', 'rotateZ(0)');
        $('html').css('overflow','auto');
        $('html').css('position','static');
        $('#userBar').hide(200);
        $('#menuBar').hide(200);
        menu = 0;
    }
});

$("textarea").keyup(function(e) {
    while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
        $(this).height($(this).height()+1);
    };
});