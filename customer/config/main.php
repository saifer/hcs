<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'customer',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'customer\controllers',
    'components' => [
        'request' => [
            'class' => \yii\web\Request::class,
            'csrfParam' => '_csrf-customer',
            'parsers' => [
                'application/json' => \yii\web\JsonParser::class,
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-customer', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the customer
            'name' => 'customer-session',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                'index' => 'site/index',
                'login' => 'site/login',
                'signup' => 'site/signup',
                'private-office' => 'site/private-office',
                'logout' => 'site/logout',

                [
                    'class' => \yii\rest\UrlRule::class,
                    'controller' => 'v1/message',
                    'pluralize' => false,
                ],
            ],
        ],
    ],
    'modules' => [
        'v1' => [
            'class' => \api\modules\v1\Module::class
        ],
    ],
    'params' => $params,
];
