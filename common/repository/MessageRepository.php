<?php

namespace common\repository;

use common\components\bot\models\Message;
use yii\db\ActiveQuery;

class MessageRepository
{
    public function findByUserIdAndDestinationToUserId(int $userId): ActiveQuery
    {
        return $this
            ->find()
            ->andWhere([
                'OR',
                ['m.user_id' => $userId],
                ['m.destination_user_id' => $userId]
            ]);
    }

    private function find(): ActiveQuery
    {
        return Message::find()
            ->alias('m');
    }
}
