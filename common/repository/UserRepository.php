<?php

namespace common\repository;

use common\models\User;
use yii\db\ActiveQuery;

class UserRepository
{
    public function getByEmail(string $email): ?User
    {
        return $this
            ->findByEmail($email)
            ->one();
    }

    private function findByEmail(string $email): ActiveQuery
    {
        return $this
            ->find()
            ->andWhere(['u.email' => $email]);
    }

    private function find(): ActiveQuery
    {
        return User::find()
            ->alias('u');
    }
}
