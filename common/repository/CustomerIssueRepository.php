<?php

namespace common\repository;

use common\components\customerIssue\models\CustomerIssue;
use yii\db\ActiveQuery;
use yii\db\BatchQueryResult;

class CustomerIssueRepository
{
    /**
     * @param int $customerId
     *
     * @return BatchQueryResult|CustomerIssue[]|[]
     */
    public function getIteratorByCustomerId(int $customerId)
    {
        return $this
            ->findByCustomerId($customerId)
            ->each();
    }

    private function findByCustomerId(int $customerId): ActiveQuery
    {
        return $this
            ->find()
            ->andWhere(['ci.customer_id' => $customerId]);
    }

    private function find(): ActiveQuery
    {
        return CustomerIssue::find()
            ->alias('ci');
    }
}
