<?php

namespace common\components\customerIssue\models;

use common\components\ActiveRecord;
use common\components\catalog\models\Catalog;
use yii\db\ActiveQuery;

/**
 * Class CustomerIssue
 * @package common\components\customerIssue\models
 * @property string $id [integer]
 * @property string $customer_id [integer]
 * @property string $trouble_id [integer]
 * @property string $comment
 * @property string $status [varchar]
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $deleted_at [timestamp]
 * @property Catalog $catalog
 */
class CustomerIssue extends ActiveRecord
{
    public const STATUS_IN_QUEUE = 'status_in_queue';
    public const STATUS_IN_WORK = 'status_in_work';
    public const STATUS_DONE = 'status_done';
    public const STATUS_CANCELED = 'status_canceled';

    public const ALL_STATUSES = [
        self::STATUS_IN_QUEUE,
        self::STATUS_IN_WORK,
        self::STATUS_DONE,
        self::STATUS_CANCELED,
    ];

    /**
     * {@inheritDoc}
     */
    public static function tableName()
    {
        return '{{%customer_issue}}';
    }

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'customer_id',
                    'trouble_id',
                    'status',
                ],
                'required',
            ],
            [
                [
                    'comment',
                    'status',
                ],
                'string',
            ],
            [
                'status',
                'in',
                'range' => self::ALL_STATUSES,
            ],
            [
                [
                    'customer_id',
                    'trouble_id',
                ],
                'integer',
            ],
        ];
    }

    /**
     * @return string
     */
    public function translateStatus(): string
    {
        $map = [
            self::STATUS_IN_QUEUE => 'В очереди',
            self::STATUS_IN_WORK => 'В работе',
            self::STATUS_DONE => 'Завершена',
            self::STATUS_CANCELED => 'Отменена',
        ];

        return $map[$this->status];
    }

    public function getCatalog(): ActiveQuery
    {
        return $this->hasOne(Catalog::class, ['id' => 'trouble_id']);
    }

    public function getHtmlClass(): string
    {
        $map = [
            self::STATUS_IN_QUEUE => '',
            self::STATUS_IN_WORK => '',
            self::STATUS_DONE => 'isSuccessMark',
            self::STATUS_CANCELED => 'isFailMark',
        ];

        return $map[$this->status];
    }
}
