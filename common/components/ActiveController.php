<?php

namespace common\components;

use Throwable;

abstract class ActiveController extends \yii\rest\ActiveController
{
    /**
     * {@inheritDoc}
     */
//    public function run($route, $params = [])
//    {
//        $response = null;
//        $status = 0;
//        $message = [];
//
//        try {
//            $response = parent::run($route, $params);
//        } catch (Throwable $exception) {
//            if (YII_ENV_DEV) {
//                throw $exception;
//            }
//
//            $response = null;
//            $status = $exception->getCode();
//            $message = [$exception->getMessage()];
//        }
//
//        return [
//            'response' => $response,
//            'status' => $status,
//            'message' => $message,
//        ];
//    }

    public function runAction($id, $params = [])
    {
        $response = null;
        $status = 0;
        $message = [];

        try {
            $response = parent::runAction($id, $params);
        } catch (Throwable $exception) {
            if (YII_ENV_DEV) {
                throw $exception;
            }

            $response = null;
            $status = $exception->getCode();
            $message = [$exception->getMessage()];
        }

        return [
            'response' => $response,
            'status' => $status,
            'message' => $message,
        ];
    }
}
