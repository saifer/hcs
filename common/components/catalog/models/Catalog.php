<?php

namespace common\components\catalog\models;

use common\components\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * Class Catalog
 * @package common\components\catalog\models
 * @property string $id [integer]
 * @property string $parent_id [integer]
 * @property string $entity [varchar(255)]
 * @property string $title [varchar(255)]
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 * @property int $deleted_at [timestamp]
 * @property Catalog $parentCategory
 */
class Catalog extends ActiveRecord
{
    public const CATEGORY = 'category';

    /**
     * {@inheritDoc}
     */
    public static function tableName()
    {
        return '{{%catalog}}';
    }

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'entity',
                    'title',
                ],
                'required',
            ],
            [
                [
                    'entity',
                    'title',
                ],
                'string',
            ],
            [
                [
                    'parent_id',
                ],
                'integer',
            ],
        ];
    }

    public function getParentCategory(): ActiveQuery
    {
        return $this->hasOne(self::class, ['id' => 'parent_id']);
    }
}
