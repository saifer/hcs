<?php

namespace common\components\profileCustomer\models;

use common\components\ActiveRecord;

/**
 * Class ProfileCustomer
 * @package common\components\profileCustomer\models
 * @property string $id [integer]
 * @property string $user_id [integer]
 * @property string $phone [varchar(255)]
 * @property int $created_at [timestamp(0)]
 * @property int $updated_at [timestamp(0)]
 * @property int $deleted_at [timestamp(0)]
 */
class ProfileCustomer extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%profile_customer}}';
    }

    public function rules()
    {
        return [
            [
                [
                    'user_id',
                    'phone',
                ],
                'required',
            ],
            [
                [
                    'phone',
                ],
                'string',
            ],
        ];
    }
}
