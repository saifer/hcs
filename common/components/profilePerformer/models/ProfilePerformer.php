<?php

namespace common\components\profilePerformer\models;

use common\components\ActiveRecord;

/**
 * Class ProfilePerformer
 * @package common\components\profilePerformer\models
 * @property string $id [integer]
 * @property string $user_id [integer]
 * @property string $company_id [integer]
 * @property string $phone [varchar(255)]
 * @property int $created_at [timestamp(0)]
 * @property int $updated_at [timestamp(0)]
 * @property int $deleted_at [timestamp(0)]
 */
class ProfilePerformer extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%profile_performer}}';
    }

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'user_id',
                ],
                'required',
            ],
            [
                [
                    'phone',
                ],
                'string',
            ],
            [
                [
                    'user_id',
                ],
                'integer',
            ]
        ];
    }
}
