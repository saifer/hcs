<?php

namespace common\components;

use yii\behaviors\TimestampBehavior;

abstract class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['created_at'],
                    self::EVENT_BEFORE_UPDATE => ['updated_ad'],
                    self::EVENT_BEFORE_DELETE => ['deleted_at'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }
}
