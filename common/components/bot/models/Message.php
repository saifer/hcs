<?php

namespace common\components\bot\models;

use common\components\ActiveRecord;
use common\models\User;

/**
 * Class Message
 * @package components\message\models
 * @property string $id [integer]
 * @property string $user_id [integer]
 * @property string $text
 * @property int $created_at [timestamp(0)]
 * @property $this $username
 * @property int $updated_at [timestamp(0)]
 * @property User $user
 * @property integer $destination_user_id [integer]
 */
class Message extends ActiveRecord
{
    /**
     * {@inheritDoc}
     */
    public static function tableName()
    {
        return '{{%message}}';
    }

    public function rules()
    {
        return [
            [
                [
                    'text',
                ],
                'required'
            ],
            [
                [
                    'text',
                ],
                'string'
            ],
            [
                [
                    'destination_user_id',
                    'user_id',
                ],
                'integer'
            ],
        ];
    }

    public function getUsername(): string
    {
        if ($this->user instanceof User) {
            $this->user->getFullName();
        }

        return BotUser::FIRST_NAME . ' ' . BotUser::LAST_NAME;
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
