<?php

namespace common\components\bot\models;

use common\components\ActiveRecord;

/**
 * Class CustomerStage
 * @package common\components\bot\models
 * @property string $user_id [integer]
 * @property string $name [varchar(255)]
 * @property string $description
 * @property int $updated_at [timestamp(0)]
 */
class CustomerStage extends ActiveRecord
{
    /**
     * {@inheritDoc}
     */
    public static function tableName()
    {
        return '{{%customer_stage}}';
    }

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'user_id',
                    'name'
                ],
                'required',
            ],
            [
                [
                    'description',
                    'name'
                ],
                'string',
            ],
            [
                [
                    'user_id',
                ],
                'string',
            ],
        ];
    }
}
