<?php

namespace common\components\user\services;

use common\components\profileCustomer\models\ProfileCustomer;
use common\exceptions\ValidationException;
use common\models\User;
use customer\models\SignupForm;
use Throwable;
use Yii;

class SignUpService
{
    /**
     * @param SignupForm $form
     *
     * @return bool
     */
    public function run(SignupForm $form)
    {
//        $transaction = new Transaction();
        try {
            if (!$form->validate()) {
                return false;
            }

            $user = new User();
            $user->email = $form->email;
            $user->first_name = $form->first_name;
            $user->last_name = $form->last_name;
            $user->setPassword($form->password);
            $user->generateAuthKey();
            $user->generateEmailVerificationToken();
            if (!$user->save()) {
                throw new ValidationException($user);
            }

            $profile = new ProfileCustomer();
            $profile->user_id = $user->id;
            $profile->phone = $form->phone;

            if (!$profile->save()) {
                throw new ValidationException($profile);
            }

            $this->sendEmail($user);

            return Yii::$app->user->login($user, true ? LoginService::SESSION_DURATION : 0);

//            $transaction->commit();
        } catch (Throwable $exception) {
//            $transaction->rollBack();

            return false;
        }
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($user->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
