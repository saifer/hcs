<?php

namespace common\components\user\services;

use common\models\LoginForm;
use common\repository\UserRepository;
use Yii;

class LoginService
{
    public const SESSION_DURATION = 3600 * 24 * 30;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function run(LoginForm $form)
    {
        if (!$form->validate()) {
            return false;
        }

        $user = $this->userRepository->getByEmail($form->email);

        return Yii::$app->user->login($user, $form->rememberMe ? self::SESSION_DURATION : 0);
    }
}
