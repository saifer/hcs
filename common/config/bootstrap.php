<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@customer', dirname(dirname(__DIR__)) . '/customer');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
