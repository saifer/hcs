<?php

namespace common\helpers;

use Yii;

class DateHelper
{
    public const FORMAT_DEFAULT = 'd.m.Y H:i';

    public static function format(string $dateTime, string $format = self::FORMAT_DEFAULT)
    {
        return Yii::$app->formatter->asDatetime($dateTime, 'php:' . $format);
    }
}
