<?php

namespace api\modules\v1\actions;

use yii\base\InvalidConfigException;
use yii\rest\Action;
use yii\web\Request;
use yii\web\User;

abstract class BaseAction extends Action
{
    /**
     * @return User|object|null
     * @throws InvalidConfigException
     */
    protected function user()
    {
        return $this->controller->module->get('user');
    }

    /**
     * @return Request|object
     * @throws InvalidConfigException
     */
    protected function request()
    {
        return $this->controller->module->get('request');
    }
}
