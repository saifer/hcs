<?php

namespace api\modules\v1\actions\message;

use api\modules\v1\actions\BaseAction;
use api\modules\v1\models\RestMessage;
use common\exceptions\ValidationException;
use common\repository\MessageRepository;
use yii\data\ActiveDataProvider;

class IndexAction extends BaseAction
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    public function __construct(
        $id,
        $controller,
        MessageRepository $messageRepository,
        $config = []
    ) {
        parent::__construct($id, $controller, $config);
        $this->messageRepository = $messageRepository;
    }

    public function run()
    {
        $user = $this->user();
        $limit = $this->request()->get('limit', false);

        $query = $this->messageRepository->findByUserIdAndDestinationToUserId($user->id);

        if (!$query->exists()) {
            /** @var RestMessage $botMessage */
            $botMessage = new $this->modelClass();
            $botMessage->destination_user_id = $user->id;
            $botMessage->text = 'Здравствуйте! Меня зовут робот Евгений, я помогу вам уладить вопросы по ЖКХ. Скажите, у вас есть какая-то проблема?';

            if (!$botMessage->save()) {
                throw new ValidationException();
            }
        }

        $query->modelClass = $this->modelClass;

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $limit,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);

        return $provider->getModels();
    }
}
