<?php

namespace api\modules\v1\actions\message;

use api\modules\v1\actions\BaseAction;
use common\exceptions\ValidationException;
use yii\base\Model;

class CreateAction extends BaseAction
{
    public $scenario = Model::SCENARIO_DEFAULT;

    public function run()
    {
        $text = $this->request()->post('text');
        $user = $this->user();

        $model = new $this->modelClass;
        $model->user_id = $user->id;
        $model->text = $text;

        if (!$model->save()) {
            throw new ValidationException($model);
        }

        return $model;
    }
}
