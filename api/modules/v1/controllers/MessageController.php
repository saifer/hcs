<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\message\CreateAction;
use api\modules\v1\actions\message\IndexAction;
use api\modules\v1\models\RestMessage;
use common\components\ActiveController;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class MessageController extends ActiveController
{
    public $modelClass = RestMessage::class;

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['options'],
                        'allow' => true,
                    ],
                ],
            ]
        ]);
    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ]);
    }
}
