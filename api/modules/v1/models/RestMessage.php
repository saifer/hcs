<?php

namespace api\modules\v1\models;

use common\components\bot\models\Message;
use Yii;

class RestMessage extends Message
{
    /**
     * {@inheritDoc}
     */
    public function fields()
    {
        return [
            'id',
            'username',
            'text',
            'created_at',
            'is_self' => function (self $model) {
                return $this->getIsSelf();
            }
        ];
    }

    public function getIsSelf(): bool
    {
        return $this->user_id == Yii::$app->user->id;
    }
}
